package com.example.starwarstechnicaltest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.starwarstechnicaltest.model.Planet;

public class PlanetDescription extends AppCompatActivity {



    public static Planet currentPlanet;

    ImageButton img_previous;
    TextView txt_planetName, txt_rotationPeriod, txt_orbitalPeriod,
            txt_diameter, txt_climate, txt_gravity, txt_terrain, txt_surface,
            txt_population;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planet_description);

        img_previous= (ImageButton) findViewById(R.id.img_previous) ;
        txt_planetName= (TextView) findViewById(R.id.txt_planetName);
        txt_rotationPeriod= (TextView) findViewById(R.id.txt_rotationPeriod);
        txt_orbitalPeriod= (TextView) findViewById(R.id.txt_orbitalPeriod);
        txt_diameter= (TextView) findViewById(R.id.txt_diameter);
        txt_climate= (TextView) findViewById(R.id.txt_climate);
        txt_gravity= (TextView) findViewById(R.id.txt_gravity);
        txt_terrain= (TextView) findViewById(R.id.txt_terrain);
        txt_surface= (TextView) findViewById(R.id.txt_surface);
        txt_population= (TextView) findViewById(R.id.txt_population);

        img_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getBaseContext(), PlanetActivity.class);
                startActivity(intent);
            }
        });


        currentPlanet = (Planet) getIntent().getSerializableExtra("PLANET");

        txt_planetName.setText(currentPlanet.getName());
        txt_climate.setText(currentPlanet.getClimate());
        txt_rotationPeriod.setText(currentPlanet.getRotation_period());
        txt_orbitalPeriod.setText(currentPlanet.getOrbital_period());
        txt_diameter.setText(currentPlanet.getDiameter());
        txt_gravity.setText(currentPlanet.getGravity());
        txt_terrain.setText(currentPlanet.getTerrain());
        txt_surface.setText(currentPlanet.getSurface_water());
        txt_population.setText(currentPlanet.getPopulation());




    }
}
