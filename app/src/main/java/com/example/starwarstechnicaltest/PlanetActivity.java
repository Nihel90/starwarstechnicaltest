package com.example.starwarstechnicaltest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.starwarstechnicaltest.adapter.PlanetAdapter;
import com.example.starwarstechnicaltest.model.Planet;
import com.example.starwarstechnicaltest.model.PlanetModel;
import com.example.starwarstechnicaltest.retrofit.IPlanetAPI;
import com.example.starwarstechnicaltest.retrofit.RetrofitPlanet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlanetActivity extends AppCompatActivity {


    @BindView(R.id.list)
    RecyclerView list;

    private PlanetAdapter planetAdapter;

    IPlanetAPI myPlanetAPI;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planet);


        init();
        getListPlanet();


    }


    private void init() {

        ButterKnife.bind(this);

        myPlanetAPI = RetrofitPlanet.getInstance().getApi();

            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            list.setLayoutManager(layoutManager);
    }


    private void getListPlanet() {

        myPlanetAPI.getPlanet().enqueue(new Callback<PlanetModel>() {
            @Override
            public void onResponse(Call<PlanetModel> call, Response<PlanetModel> response) {

                displayList(response.body().getResults());

            }
            @Override
            public void onFailure(Call<PlanetModel> call, Throwable t) {
                Toast.makeText(PlanetActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


   private void displayList(List<Planet> planets){

        PlanetAdapter adapter = new PlanetAdapter(PlanetActivity.this, planets);
        list.setAdapter(adapter);
    }





}