package com.example.starwarstechnicaltest.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.starwarstechnicaltest.PlanetDescription;
import com.example.starwarstechnicaltest.R;
import com.example.starwarstechnicaltest.model.Planet;
import com.example.starwarstechnicaltest.model.PlanetModel;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class PlanetAdapter extends RecyclerView.Adapter<PlanetAdapter.MyViewHolder> {


    public static Planet currentPlanet=null;
    Context context;
    List<Planet> planetList;

    public PlanetAdapter(Context context, List<Planet> planetList) {
        this.context = context;
        this.planetList = planetList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.layout_planet, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {



        holder.txt_planetName.setText(planetList.get(position).getName());
        holder.txt_planetDiameter.setText(planetList.get(position).getDiameter());
        holder.txt_planetPopulation.setText(planetList.get(position).getPopulation());



        holder.setListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position) {

                currentPlanet = planetList.get(position);

                Intent intent = new Intent(context, PlanetDescription.class);

                Planet planet= new Planet (currentPlanet.getName(), currentPlanet.getClimate(), currentPlanet.getGravity(),
                        currentPlanet.getTerrain(), currentPlanet.getRotation_period(), currentPlanet.getOrbital_period(),
                        currentPlanet.getDiameter(), currentPlanet.getSurface_water(), currentPlanet.getPopulation(),
                        currentPlanet.getResidents(), currentPlanet.getFilms(), currentPlanet.getCreated(), currentPlanet.getEdited(),
                        currentPlanet.getUrl());



                intent.putExtra("PLANET", planet);
                context.startActivity(intent);


            }
        });


    }

    @Override
    public int getItemCount() {
        return planetList.size();     }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        @BindView(R.id.txt_planetName)
        TextView txt_planetName;
        @BindView(R.id.txt_planetPopulation)
        TextView txt_planetPopulation;
        @BindView(R.id.txt_planetDiameter)
        TextView txt_planetDiameter;


        ItemClickListener listener;


        public void setListener(ItemClickListener listener) {

            this.listener = listener;
        }

        Unbinder unbinder;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder= ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            listener.onClick(view, getAdapterPosition());
        }

    }


}
