package com.example.starwarstechnicaltest.retrofit;

import com.example.starwarstechnicaltest.model.Planet;
import com.example.starwarstechnicaltest.model.PlanetModel;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;

public interface IPlanetAPI {


@GET("planets")
    Call<PlanetModel>  getPlanet();


}
