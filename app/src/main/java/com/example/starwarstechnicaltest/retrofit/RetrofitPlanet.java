package com.example.starwarstechnicaltest.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitPlanet {

    private static final String BASE_URL = "https://swapi.co/api/";
    private static RetrofitPlanet mInstance;
    private Retrofit retrofit;


    public RetrofitPlanet() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


    public static synchronized RetrofitPlanet getInstance() {
        if (mInstance == null) {
            mInstance = new RetrofitPlanet();
        }
        return mInstance;
    }


    public IPlanetAPI getApi() {

        return retrofit.create(IPlanetAPI.class);
    }



}
